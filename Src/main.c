/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include "EtherShield.h"
#define NET_BUF_SIZE (1<<10)
#define UART_BUF_SIZE (NET_BUF_SIZE << 2)

#define MAX(a,b) ((a)>(b)?(a):(b))
#define BUF_SIZE MAX(NET_BUF_SIZE, UART_BUF_SIZE)

uint8_t globalWatch;
uint16_t globalWatch1;
uint16_t globalWatch2;
uint16_t globalWatch3;
uint16_t globalWatch4;

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;
DMA_HandleTypeDef hdma_spi1_tx;
DMA_HandleTypeDef hdma_spi1_rx;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_SPI1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


static inline void blink(int times, int delay) {
	int i = 0;
	while (i++ < times) {
		GPIOA->BSRR = GPIO_PIN_5;
		HAL_Delay(delay);
		GPIOA->BSRR = GPIO_PIN_5 << 16;
		HAL_Delay(delay);
	}

	return;
}

void ES_PingCallback(void) {
}

uint16_t get_udp_data_len(uint8_t * buf)
{
	int16_t i;
	i = (((int16_t)buf[IP_TOTLEN_H_P]) << 8) | (buf[IP_TOTLEN_L_P] & 0xff);
	i -= IP_HEADER_LEN;
	i -= 8;
	if (i <= 0) {
		i = 0;
	}
	return ((uint16_t)i);
}

static uint16_t info_data_len = 0;
uint16_t packetloop_icmp_udp(uint8_t * buf, uint16_t plen)
{
	if (eth_type_is_arp_and_my_ip(buf, plen)) {
		if (buf[ETH_ARP_OPCODE_L_P] == ETH_ARP_OPCODE_REQ_L_V) {
			// is it an arp request 
			make_arp_answer_from_request(buf);
		}
		return (0);

	}
	// check if ip packets are for us:
	if(eth_type_is_ip_and_my_ip(buf, plen) == 0) {
		return (0);
	}

	if (buf[IP_PROTO_P] == IP_PROTO_ICMP_V && buf[ICMP_TYPE_P] == ICMP_TYPE_ECHOREQUEST_V) {
		make_echo_reply_from_request(buf, plen);
		return (0);
	}

	if (buf[IP_PROTO_P] == IP_PROTO_UDP_V) {
		info_data_len = get_udp_data_len(buf);
		return (IP_HEADER_LEN + 8 + 14);
	}

	return (0);
}

uint8_t your_client_tcp_result_callback(uint8_t fd,
	uint8_t statuscode, 
	uint16_t data_start_pos_in_buf,
	uint16_t len_of_data)
{
	asm("BKPT");
	if (statuscode != 0)
	{
		
		
	}
	
	
	return 1;
//	return close_tcp_session;
}

uint16_t your_client_tcp_datafill_callback(uint8_t fd)
{
	asm("BKPT");
	
	
//	...your code; 
	
	return (3);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_SPI1_Init();
	/* USER CODE BEGIN 2 */
	
	static uint8_t uart_buf[UART_BUF_SIZE], * uart_buf_parsed, * uart_buf_filled, * uart_buf_end;
	
	uint16_t netanswer_initialpacket_len = 0;
	uint8_t  mac[] = { 02, 03, 04, 05, 06, 07 };
	uint8_t  ip[] = { 192, 168, 1, 209 };
	
	static uint8_t net_buf[NET_BUF_SIZE + 1];
	static uint8_t netanswer_buf[NET_BUF_SIZE + 1];
	
	
	//Init Funstions
	ES_enc28j60SpiInit(&hspi1);
	ES_enc28j60Init(mac);

	uint8_t enc28j60_rev = ES_enc28j60Revision();
	globalWatch = enc28j60_rev;

	if (enc28j60_rev <= 0)
	{
		asm("BKPT");
		//		some error occured, handle it
	}

	ES_init_ip_arp_udp_tcp(mac, ip, 80);
	
	uint8_t buf[100] = "ajslkdjfasdf";
	uint16_t dat_p;
	uint16_t outlen = 12;
	uint16_t plen = 8;
	//Connecting to the server
	uint8_t ipAddr[] = { 192, 168, 1, 219 };
	ES_client_tcp_set_serverip(ipAddr);
	
	
	
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		globalWatch2 = ES_packetloop_icmp_tcp(buf, plen);
		globalWatch3 = ES_client_tcp_req(your_client_tcp_result_callback, your_client_tcp_datafill_callback, 80);
//		globalWatch3 = ES_client_tcp_req(uint8_t(*result_callback)(uint8_t fd, uint8_t statuscode, uint16_t data_start_pos_in_buf, uint16_t len_of_data), uint16_t(*datafill_callback)(uint8_t fd), uint16_t port);

//		ES_tcp_client_send_packet(buf, 80, 80, 3, 5, 3, 2, 3, mac, ip);
//ES_tcp_client_send_packet(, uint8_t flags, uint8_t max_segment_size, 
//	uint8_t clear_seqck, uint16_t next_ack_num, uint16_t dlength, uint8_t *dest_mac, uint8_t *dest_ip){
		
		globalWatch1++;
		
		
		//		if (netanswer_initialpacket_len != 0) {
		//			memcpy(net_buf, netanswer_buf, netanswer_initialpacket_len);
		//			make_udp_reply_from_request(net_buf, (char*)uart_buf_parsed, outlen, 23);
		//		}

		
		//		dat_p = packetloop_icmp_udp(net_buf, ES_enc28j60PacketReceive(NET_BUF_SIZE, net_buf));

		
		//		if (dat_p == 0)
		//			continue;
		//		netanswer_initialpacket_len = dat_p + info_data_len;
		//		memcpy(netanswer_buf, net_buf, dat_p + info_data_len);

		    /* USER CODE END WHILE */

		    /* USER CODE BEGIN 3 */
	  
				//	  BOOL            xMBTCPPortInit(USHORT usTCPPort);
				//
				//	  void            vMBTCPPortClose(void);
				//
				//	  void            vMBTCPPortDisable(void);
				//
				//	  BOOL            xMBTCPPortGetRequest(UCHAR **ppucMBTCPFrame, USHORT * usTCPLength);
				//
				//	  BOOL            xMBTCPPortSendResponse(const UCHAR *pucMBTCPFrame, USHORT usTCPLength);

				//		void ES_make_echo_reply_from_request(uint8_t *buf, uint16_t len);
				//		void ES_make_tcp_synack_from_syn(uint8_t *buf);
				//		void ES_make_tcp_ack_from_any(uint8_t *buf, int16_t datlentoack, uint8_t addflags);
				//		void ES_make_tcp_ack_with_data(uint8_t *buf, uint16_t dlen);
				//		void ES_make_tcp_ack_with_data_noflags(uint8_t *buf, uint16_t dlen);
				//		void ES_init_len_info(uint8_t *buf);
				//		uint16_t ES_get_tcp_data_pointer(void);
				//		uint16_t ES_build_tcp_data(uint8_t *buf, uint16_t srcPort);
				//		void ES_send_tcp_data(uint8_t *buf, uint16_t dlen);
	
	}
	/* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Initializes the CPU, AHB and APB busses clocks 
	*/
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB busses clocks 
	*/
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
	                            | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		Error_Handler();
	}
	/** Enables the Clock Security System 
	*/
	HAL_RCC_EnableCSS();
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

	/* USER CODE BEGIN SPI1_Init 0 */

	/* USER CODE END SPI1_Init 0 */

	/* USER CODE BEGIN SPI1_Init 1 */

	/* USER CODE END SPI1_Init 1 */
	/* SPI1 parameter configuration*/
	hspi1.Instance = SPI1;
	hspi1.Init.Mode = SPI_MODE_MASTER;
	hspi1.Init.Direction = SPI_DIRECTION_2LINES;
	hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi1.Init.NSS = SPI_NSS_SOFT;
	hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
	hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi1.Init.CRCPolynomial = 10;
	if (HAL_SPI_Init(&hspi1) != HAL_OK)
	{
		Error_Handler();
	}
	/* USER CODE BEGIN SPI1_Init 2 */

	/* USER CODE END SPI1_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
	/* DMA controller clock enable */
	__HAL_RCC_DMA1_CLK_ENABLE();

	/* DMA interrupt init */
	/* DMA1_Channel2_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
	/* DMA1_Channel3_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);

	/*Configure GPIO pin : PC13 */
	GPIO_InitStruct.Pin = GPIO_PIN_13;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : PA1 */
	GPIO_InitStruct.Pin = GPIO_PIN_1;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	  /* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
	/* USER CODE BEGIN 6 */
	  /* User can add his own implementation to report the file name and line number,
	     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
